from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    return jsonify(books)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('show_books.html', books=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        book = {'title': request.form['title'], 'id': str(random.randint(100, 999))}
        books.append(book)
        return redirect(url_for('showBook'))
    else:
        return render_template('new_book.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    book = [book for book in books if book['id'] == str(book_id)][0]
    if request.method == 'POST':
        book['title'] = request.form['title']
        return redirect(url_for('showBook'))
    else:
        return render_template('edit_book.html', book=book)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    book = [book for book in books if book['id'] == str(book_id)][0]
    if request.method == 'POST':
        books.remove(book)
        return redirect(url_for('showBook'))
    else:
        return render_template('delete_book.html', book=book)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
